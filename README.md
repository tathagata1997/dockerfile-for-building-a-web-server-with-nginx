# Dockerfile for Building a Web Server with Nginx



## Overview

This Dockerfile is part of an exercise where the goal is to build a containerized web server using Nginx. The container is intended to be built and invoked using a specific procedure outlined below.

## Usage

To build and run the Docker container, follow the steps below:

- Change directory to the feature152 folder:

```
cd Dockerfile for Building a Web Server with Nginx
```

- Build the Docker image with the provided Dockerfile:

```
docker build -t feature152.main -f Dockerfile.main.df .
```

- Run the Docker container, giving it a name 'server.local':

```
docker run --rm --name server.local feature152.main /bin/sh
```

- Start the web server inside the running container (replace <a commmand to start the web server> with the appropriate command):

```
docker exec server.local <a commmand to start the web server>
```
Note: No Docker ENTRYPOINT or CMD directives are needed.


## Git Workflow

- Stage, commit, and push your changes, including the Dockerfile, to the feature branch 'feature152'.
- Verify in the Gitea Web application that the green check-mark appears at the feature branch 'feature152'.
- If successful, submit a pull request via the Gitea Web application.


## Success Criteria

The running Docker image will be checked for the following:

- Using the correct Web server to serve Web pages (Nginx).
- Running on Ubuntu 22.04.
- Serving the Web server's default welcome page when called under [ http://server.local]( http://server.local)


## Resources

For more information on Docker and Dockerfile, refer to the following resources:

- [ Docker Get Started Guide ]( https://docs.docker.com/get-started/)
- [ Dockerfile Reference ]( https://docs.docker.com/engine/reference/builder/)
- Read the official Docker documentation for detailed insights.

